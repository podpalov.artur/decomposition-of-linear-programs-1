import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Enter first natural number");
        while (!scan.hasNextInt()) {
            scan.nextLine();
            System.out.println("Enter first natural number");
        }
        int a = scan.nextInt();

        System.out.println("Enter second natural number");
        while (!scan.hasNextInt()) {
            scan.nextLine();
            System.out.println("Enter second natural number");
        }
        int b = scan.nextInt();

        System.out.println("Greatest common divisor = " + gcd(a, b));
        System.out.println("Smallest common multiple = " + scm(a, b));

        System.out.println("Enter third natural number");
        while (!scan.hasNextInt()) {
            scan.nextLine();
            System.out.println("Enter third natural number");
        }
        int c = scan.nextInt();

        System.out.println("Smallest common multiple of 3 numbers = " + scm3(a, b, c));

        System.out.println("Enter fourth natural number");
        while (!scan.hasNextInt()) {
            scan.nextLine();
            System.out.println("Enter fourth natural number");
        }
        int d = scan.nextInt();

        System.out.println("Greatest common divisor of 4 numbers = " + gcd4(a, b, c, d));
    }

    public static int gcd(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    public static int scm(int a, int b) {
        return (a * b) / gcd(a, b);
    }

    public static int gcd4(int a, int b, int c, int d) {
        return gcd(gcd(gcd(a, b), c), d);
    }

    public static int scm3(int a, int b, int c) {
        return scm(scm(a, b), c);
    }
}


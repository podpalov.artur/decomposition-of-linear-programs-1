import java.util.Scanner;

public class Factorial {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the odd number from 1 to 9");
        while (!scan.hasNextInt()) {
            scan.nextLine();
            System.out.println("Enter the odd number from 1 to 9");
        }
        int f = scan.nextInt();
        System.out.println("Factorial of " + f + " = " + factorial(f));
        System.out.println("Sum of factorials of all odd numbers from 1 to 9 = " + sumOfOddNumbers());
    }

    public static int factorial(int a) {
        int f = 1;
        for (int i = 1; i <= a; i++) {
            f = f * i;
        }
        return f;
    }

    public static int sumOfOddNumbers() {
        int sum = 0;
        for (int i = 1; i <= 9; i += 2) {
            sum += factorial(i);
        }
        return sum;
    }
}

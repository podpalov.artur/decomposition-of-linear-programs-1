import java.util.Arrays;
import java.util.Scanner;

public class SecondMax {
    public static void main(String[] args) {
        System.out.println("Enter the length of the array");
        Scanner scanner = new Scanner(System.in);
        int length = scanner.nextInt();
        int[] array = new int[length];
        System.out.println("Fill the array with " + length + " natural numbers");
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
        System.out.println(findSecondMax(array));
    }

    public static int findSecondMax(int[] array) {
        int max = array[0];
        for (int i = 1; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        int secondMax = array[0];
        for (int i = 1; i < array.length; i++) {
            if (secondMax < array[i] & array[i] != max) {
                secondMax = array[i];
            }
        }
        return secondMax;
    }
}
